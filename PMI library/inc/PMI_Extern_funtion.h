#ifndef PMI_extern_funtion_H
#define PMI_extern_funtion_H

#include "PMI.h"

#define SWITCH
#define BUTTON
#define Z_DETECT


/*Switch funtion define*/
#define SWITCH_PIN  	(GPIO_Pin_1 | GPIO_Pin_11 | GPIO_Pin_10)
#define SWITCH_STT  	(SWITCH_PORT->IDR & SWITCH_PIN)
#define SWITCH_PORT		 GPIOB

#define HANDJOB     ((SWITCH_PORT->IDR & GPIO_Pin_12) == 0x1000)
#define MATUMBA     ((SWITCH_PORT->IDR & GPIO_Pin_12) == 0x00)
#define AUTO_Z		((SWITCH_PORT->IDR & GPIO_Pin_11) == 0x800)
#define MANUAL      0//(SWITCH_STT == 0x00) 

/*Button funtion define*/
#define BUTTON_FUNC_PIN  	 GPIO_Pin_15
#define BUTTON_FUNC_PORT		 GPIOA

#define BUTTON_FUNC_STT  	(BUTTON_FUNC_PORT->IDR & BUTTON_FUNC_PIN)
#define BUTTON_FUNC_TRUE  	 BUTTON_FUNC_STT == 0x00
#define BUTTON_FUNC_FALSE  	 BUTTON_FUNC_STT != 0x00

#define LED_BT_FUNC_PIN		 GPIO_Pin_12
#define LED_BT_PORT			 GPIOA
#define LED_BT_ON			 LED_BT_PORT->ODR |= LED_BT_FUNC_PIN
#define LED_BT_OFF			 LED_BT_PORT->ODR &= ~LED_BT_FUNC_PIN

/*Button reGPIO_InitStructset position define*/
#define BUTTON_RST_POS_PIN  	 GPIO_Pin_4
#define BUTTON_RST_POS_PORT	 GPIOB

#define BUTTON_RST_POS_STT  	(BUTTON_RST_POS_PORT->IDR & BUTTON_RST_POS_PIN)
#define BUTTON_RST_POS_TRUE  	 BUTTON_RST_POS_STT == 0x00
#define BUTTON_RST_POS_FALSE   BUTTON_RST_POS_STT != 0x00

#define LED_BT_RST_POS_PIN	 GPIO_Pin_3
#define LED_BT_RST_POS_PORT	 GPIOB
#define LED_BT_RST_POS_ON	 LED_BT_RST_POS_PORT->ODR |= LED_BT_RST_POS_PIN
#define LED_BT_RST_POS_OFF	 LED_BT_RST_POS_PORT->ODR &= ~LED_BT_RST_POS_PIN


/*Z detector funtion define*/

#define Z_DETECT_PIN     GPIO_Pin_15
#define Z_DETECT_PORT    GPIOC

/*Funtion*/
void Exter_func_config();
void GPIO_Z_dectect_Init();
void RCC_init();
void GPIO_SW_Init();
void GPIO_Button_Init();
void Extern_funtion_init();
void Reset_to_zeros_position();
void debug_led();
int	Auto_z_leveling_Point(hcode des, bool Back_to_original_position);
void Auto_z_leveling_Matrix(int **Auto_Z_matrix,hcode hc);
void Back_to_zeros_position();
void No_Step_to_Copper_Calculator();
void Change_tool();
void _delay(int32_t _counter);
void waiting_is_happy();
#endif /*PMI_extern_funtion_H*/
