#ifndef PMI_Hcode_H
#define PMI_Hcode_H

#include <stdbool.h>
#include <stdint.h>
#define NUMB_OF_MOTOR (uint8_t) 3
typedef enum
{
     step_x = 0,
     step_y,
     step_z
}hc_step;
typedef enum
{
     ACT_NORMAL 	= 0,
     ACT_FASTMOVE 	= 1,
     ACT_STOP 		= -1,
	ACT_DRILL 	= 68,
	ACT_MILL		= 77,
	ACT_CUT		= 67,
	ACT_SIDE		= 83,
	ACT_Z_LEVELING	= 90,
	IN_THE_END	= 69
}act;
/*
step[0] = step[step_y]
step[1] = step[step_x]
step[2] = step[step_z]

#define MotorY_Timer     TIM1
#define MotorX_Timer     TIM2
#define MotorZ_Timer     TIM3
*/
typedef struct 
{
	int32_t step[3];
	int action;
     int32_t CID;
}hcode;
typedef struct
{
     char hcode_string[30];
}hcode_stringtype;
void hc_init(hcode* input);
void hc_init_vl(hcode* input,int act,int x, int y, int z);
hcode hc_string_to_hcode(char* input);
float hc_string_to_real(char* input);
int  hc_string_to_numb(char* input);
void empty_string(char* input);

void hc_init_auto_z_Xaxis(hcode* input);
void hc_init_auto_z_Yaxis(hcode* input);
void hc_init_auto_z_Zaxis(hcode* input, int step);
#endif /*PMI_Hcode_H*/
