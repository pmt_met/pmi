#ifndef PMI_Motor_Ctrl_H
#define PMI_Motor_Ctrl_H
#include "PMI_Hcode.h"
/**/

/*GPIO Port/Pin define */

	/*X (A8-B15)*/
#define GPIO_PWM_X_PORT    GPIOA           
#define GPIO_PWM_X_PIN	  GPIO_Pin_8

#define GPIO_DIR_X_PORT    GPIOB
#define GPIO_DIR_X_PIN	  GPIO_Pin_15

	/*Y (A0-A1)*/
#define GPIO_PWM_Y_PORT    GPIOA           
#define GPIO_PWM_Y_PIN	  GPIO_Pin_0

#define GPIO_DIR_Y_PORT    GPIOA
#define GPIO_DIR_Y_PIN	  GPIO_Pin_1

	/*Z (B6-B5)*/
#define GPIO_PWM_Z_PORT    GPIOB           
#define GPIO_PWM_Z_PIN	  GPIO_Pin_6

#define GPIO_DIR_Z_PORT    GPIOB
#define GPIO_DIR_Z_PIN	  GPIO_Pin_5

/*TIMER DIFINE*/
#define MotorX_Timer     TIM1
#define MotorY_Timer     TIM2
#define MotorZ_Timer     TIM4

/*Funtion macro*/

#define MOVING_LEFT		GPIO_DIR_X_PORT->ODR |= GPIO_DIR_X_PIN
#define MOVING_RIGHT	GPIO_DIR_X_PORT->ODR &= ~GPIO_DIR_X_PIN
#define REVERSE_DIR_X	GPIO_DIR_X_PORT->ODR ^= GPIO_DIR_X_PIN

#define MOVING_FORWARD	GPIO_DIR_Y_PORT->ODR |= GPIO_DIR_Y_PIN
#define MOVING_BACKWARD	GPIO_DIR_Y_PORT->ODR &= ~GPIO_DIR_Y_PIN
#define REVERSE_DIR_Y	GPIO_DIR_Y_PORT->ODR ^= GPIO_DIR_Y_PIN

#define MOVING_UP		GPIO_DIR_Z_PORT->ODR &= ~GPIO_DIR_Z_PIN	
#define MOVING_DOWN		GPIO_DIR_Z_PORT->ODR |= GPIO_DIR_Z_PIN
#define REVERSE_DIR_Z	GPIO_DIR_Z_PORT->ODR ^= GPIO_DIR_Z_PIN
/*-----------------------------------*/
typedef enum
{
	_move_left = 0,
	_move_right,
	_move_forward,
	_move_backward,
	_move_up,
	_move_down,
}moving_direction;

#define MOVING_STEP		1600
/*--------------------------------------*/
void PWM_RCC_Init(void);
void PWM_GPIO_Init(void);
void PWM_Init(void);
void PWM_NVIC_Configuration(void);
void PWM_Move(int dir);



/*-----------------------------------*/

void PWM_Enforce_Hcode(hcode hc);

#endif /*PMI_Motor_Ctrl_H*/
