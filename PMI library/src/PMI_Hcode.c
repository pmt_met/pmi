#include "PMI.h"

/*reset hcode'value to zero*/
void hc_init(hcode* input)
{
     input->action = 0;
     input->step[step_x] = 0;
     input->step[step_y] = 0;
     input->step[step_z] = 0;
     input->CID = 0;
}
void hc_init_vl(hcode* input,int act,int x, int y, int z)
{
     input->action = act;
     input->step[step_x] = x;
     input->step[step_y] = y;
     input->step[step_z] = z;
     input->CID = 0;
}
void hc_init_auto_z_Xaxis(hcode* input)
{
     input->action = 0;
     input->step[step_x] = AUTO_Z_SAMPLING_STEP;
     input->step[step_y] = 0;
     input->step[step_z] = 0;
     input->CID = 0;
}
void hc_init_auto_z_Yaxis(hcode* input)
{
     input->action = 0;
     input->step[step_x] = 0;
     input->step[step_y] = AUTO_Z_SAMPLING_STEP;
     input->step[step_z] = 0;
     input->CID = 0;
}
void hc_init_auto_z_Zaxis(hcode* input, int step)
{
     input->action = 0;
     input->step[step_x] = 0;
     input->step[step_y] = 0;
     input->step[step_z] = step;
     input->CID = 0;
}
float hc_string_to_real(char* input)
{
	int i = 0, sign = 1;
	float result = 0;
	while (input[i])
	{
		if (input[i] == '-')
			sign = -1;
		else
		{
			/*if(input[i] == '.')
				//dot = i;
			else */
			if (input[i] != '.')
				result = result * 10 + input[i]-48;
			
		}
		i++;
	}
	return sign*result / 10000;
}
int hc_string_to_numb(char* input)
{
	int i = 0, sign = 1;
	int result = 0;
	while (input[i])
	{
		if (input[i] == '-')
			sign = -1;
		else
               result = result * 10 + input[i]-48;
		i++;
	}
	return sign*result;
}

hcode hc_string_to_hcode(char* input)
{
	
	int i = 0, j = 0, k = 0;
	char temp[20] = "";
	hcode result;
     
	hc_init(&result); //reset for sure :V

	while(input[i] != ' ') // read command id until get space character
	{
		result.CID = result.CID *10 + (input[i] - '0'); 
		i++;
	}
	
	i++;
	result.action = (input[i] - '0')*10 + input[i+1] - '0';
	i += 3;
	
	while(input[i] != '.') //end character
	{
		if(input[i] == '-' || (input[i] >= '0' && input[i] <= '9'))
			temp[j] = input[i];
		if(input[i+1] == 'Y' || input[i+1] == 'Z' || !input[i+1] || input[i+1] == '.')
		{
			result.step[k] = hc_string_to_numb(temp);
			k++;
			j = 0;
			i++;
			empty_string(temp);
		}
		else
			j++;
		if(input[i] != '.')
			i++;
	}
     
     return result;
}
void empty_string(char* input)
{
     int i = 0;
     while(input[i])
     {
          input[i] = 0;
          i++;
     }
}



