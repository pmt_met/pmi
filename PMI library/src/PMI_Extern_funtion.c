#include "PMI_Extern_funtion.h"


void Extern_funtion_init()
{
     RCC_init();
#ifdef SWITCH
     GPIO_SW_Init();
#endif
     
#ifdef Z_DETECT
     GPIO_Z_dectect_Init();
#endif
     
#ifdef BUTTON
     GPIO_Button_Init();
	Reset_to_zeros_position();
#endif
}

void GPIO_Z_dectect_Init()
{
     /*declare variable*/
     EXTI_InitTypeDef   EXTI_Z_detect;
     GPIO_InitTypeDef   GPIO_Z_detect;
     NVIC_InitTypeDef   NVIC_Z_detect;
     
     /*GPIO init for z detector*/
     GPIO_Z_detect.GPIO_Mode = GPIO_Mode_IPU;
     GPIO_Z_detect.GPIO_Pin = Z_DETECT_PIN;
     GPIO_Z_detect.GPIO_Speed = GPIO_Speed_50MHz;

     GPIO_Init(Z_DETECT_PORT,&GPIO_Z_detect);
     
     /* Extern interupt init for z detector*/
     GPIO_EXTILineConfig(GPIO_PortSourceGPIOC,GPIO_PinSource15);
     
     EXTI_Z_detect.EXTI_Line = EXTI_Line15;
     EXTI_Z_detect.EXTI_Mode = EXTI_Mode_Interrupt;
     EXTI_Z_detect.EXTI_Trigger = EXTI_Trigger_Falling;
     EXTI_Z_detect.EXTI_LineCmd = ENABLE;
     EXTI_Init(&EXTI_Z_detect);
     
     NVIC_Z_detect.NVIC_IRQChannel = EXTI15_10_IRQn;
     NVIC_Z_detect.NVIC_IRQChannelPreemptionPriority = 2;
     NVIC_Z_detect.NVIC_IRQChannelSubPriority = 2;
     NVIC_Z_detect.NVIC_IRQChannelCmd = ENABLE;
     NVIC_Init(&NVIC_Z_detect);
}
void RCC_init()
{
     RCC_APB2PeriphClockCmd(RCC_APB2Periph_AFIO, ENABLE);
     RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB | RCC_APB2Periph_GPIOC, ENABLE);
}
void GPIO_SW_Init()
{
     GPIO_InitTypeDef sw;
     
     sw.GPIO_Mode = GPIO_Mode_IN_FLOATING;
     sw.GPIO_Pin =  SWITCH_PIN;
     
     GPIO_Init(SWITCH_PORT, &sw);
}
void GPIO_Button_Init()
{
     GPIO_InitTypeDef bt;
     GPIO_InitTypeDef led;
	
	/**/
	GPIO_PinRemapConfig(GPIO_Remap_SWJ_JTAGDisable,ENABLE);
	/**/
     bt.GPIO_Mode = GPIO_Mode_IN_FLOATING;
     bt.GPIO_Pin  =  BUTTON_FUNC_PIN;
     
     GPIO_Init(BUTTON_FUNC_PORT, &bt);
	
	led.GPIO_Mode = GPIO_Mode_Out_PP;
	led.GPIO_Pin = GPIO_Pin_12;
	led.GPIO_Speed = GPIO_Speed_50MHz;
	
	GPIO_Init(GPIOA, &led);
	
	LED_BT_OFF;
}

void Reset_to_zeros_position()
{
	/*declare variable*/
     EXTI_InitTypeDef   EXTI_RST_pos;
     GPIO_InitTypeDef   GPIO_RST_pos;
     NVIC_InitTypeDef   NVIC_RST_pos;
     
     /*GPIO init for z detector*/
     GPIO_RST_pos.GPIO_Mode = GPIO_Mode_IPU;
     GPIO_RST_pos.GPIO_Pin = BUTTON_RST_POS_PIN;
     

     GPIO_Init(BUTTON_RST_POS_PORT,&GPIO_RST_pos);
     
	GPIO_RST_pos.GPIO_Mode = GPIO_Mode_Out_PP;
	GPIO_RST_pos.GPIO_Pin = LED_BT_RST_POS_PIN;
	GPIO_RST_pos.GPIO_Speed = GPIO_Speed_50MHz;
	
	GPIO_Init(LED_BT_RST_POS_PORT,&GPIO_RST_pos);
     /* Extern interupt init for z detector*/
     GPIO_EXTILineConfig(GPIO_PortSourceGPIOB,GPIO_PinSource4);
     
     EXTI_RST_pos.EXTI_Line = EXTI_Line4;
     EXTI_RST_pos.EXTI_Mode = EXTI_Mode_Interrupt;
     EXTI_RST_pos.EXTI_Trigger = EXTI_Trigger_Falling;
     EXTI_RST_pos.EXTI_LineCmd = ENABLE;
     EXTI_Init(&EXTI_RST_pos);
     
     NVIC_RST_pos.NVIC_IRQChannel = EXTI4_IRQn;
     NVIC_RST_pos.NVIC_IRQChannelPreemptionPriority = 0;
     NVIC_RST_pos.NVIC_IRQChannelSubPriority = 2;
     NVIC_RST_pos.NVIC_IRQChannelCmd = ENABLE;
     NVIC_Init(&NVIC_RST_pos);
}

void debug_led()
{
     GPIO_InitTypeDef led;
     
     RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOC, ENABLE);
     led.GPIO_Mode = GPIO_Mode_Out_PP;
     led.GPIO_Pin = GPIO_Pin_13;
     led.GPIO_Speed = GPIO_Speed_50MHz;
     
     GPIO_Init(GPIOC,&led);
     GPIOC->ODR &= ~GPIO_Pin_13;
}

int	Auto_z_leveling_Point(hcode des, bool Back_to_original_position)
{
	extern volatile bool Z_axis_get_zeros;
	extern int32_t Auto_z_counter;
	/*Number of step to copper*/
	int32_t No_step_to_copper = 0; 
	
	/*Moving to destinations*/
	PWM_Enforce_Hcode(des);
	while(!left_step_equal_zeros());
	
	/*Turn down Z axis until get copper board*/
	z_reset();
	MOVING_DOWN;
	while(!Z_axis_get_zeros)
	{
		TIM_Cmd(MotorZ_Timer,ENABLE);
	}
	
	/*Calculator number of step to get to copper*/
	No_step_to_copper = Auto_z_counter;
	
	
	/*Turn up Z axis*/
	
	if(Back_to_original_position)
	{
		hc_init_auto_z_Zaxis(&des, No_step_to_copper);
		No_step_to_copper = - AUTO_Z_STEP_Z + No_step_to_copper;
	}
	else
	{
		hc_init_auto_z_Zaxis(&des, 0);
		//hc_init_auto_z_Zaxis(&des, AUTO_Z_STEP_Z);
		No_step_to_copper = 0;
	}
	PWM_Enforce_Hcode(des);
	while(!left_step_equal_zeros());
	
	/**/
	return No_step_to_copper;
}
void Auto_z_leveling_Matrix(int **matrix,hcode hc)
{
	int height, width;
	//int matrix[15][15];
	int i, j;
			
	height = hc.step[step_y];
	width  = hc.step[step_z];
	
	matrix = (int**)calloc(height,sizeof(int));
	for(i = 0; i < height; i++)
	{
		matrix[i] = (int*)calloc(width,sizeof(int));
	}
	
	for(i = 0; i < height; i++)
	{
		for(j = 0; j < width; j++)
		{
			/*Zero postion*/
			if(0 == i && 0 == j)
			{
				hc_init(&hc);
				matrix[i][j] = Auto_z_leveling_Point(hc, false);
			}
			/*First of col*/
			else if(0 == j)
			{
				hc_init(&hc);
				matrix[i][j] = Auto_z_leveling_Point(hc, true);
			}
			/*Rest of world*/
			else
			{
				hc_init_auto_z_Xaxis(&hc);
				matrix[i][j] = Auto_z_leveling_Point(hc, true);
			}
		}
		if( i >= (height - 1))
			break;
		/*cumback to 1st column*/
		hc_init_vl(&hc,0,- (height - 1)*AUTO_Z_SAMPLING_STEP,0,0);
		PWM_Enforce_Hcode(hc);
		while(!left_step_equal_zeros());
		
		/*move to next row*/
		hc_init_auto_z_Yaxis(&hc);
		PWM_Enforce_Hcode(hc);
		while(!left_step_equal_zeros());
	}
	Back_to_zeros_position();
	
}
void Back_to_zeros_position()
{
	int x, y, z;
	hcode hc;
	extern int32_t Motor_Postion[3];
	/**/
	x = Motor_Postion[step_x];
	y = Motor_Postion[step_y];
	z = Motor_Postion[step_z];
	
	/**/
	hc_init_vl(&hc,0,-x,0,0);
	PWM_Enforce_Hcode(hc);
	while(!left_step_equal_zeros());
	
	hc_init_vl(&hc,0,0,-y,0);
	PWM_Enforce_Hcode(hc);
	while(!left_step_equal_zeros());
	
	/**/
	hc_init_vl(&hc,0,0,0,-(Z_HIGHT_CHANGETOOL - z));
	PWM_Enforce_Hcode(hc);
	while(!left_step_equal_zeros());
}

void No_Step_to_Copper_Calculator()
{
	
}

void Change_tool()
{
	/*Handjob*/
	
	hcode a;
	hc_init_vl(&a,0,0,0,Z_HIGHT_CHANGETOOL);
	UART_SendStr("\nChanging tool");
	PWM_Enforce_Hcode(a);
	while(!left_step_equal_zeros());
	UART_SendStr("..Done");
	/*GOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO*/
	waiting_is_happy();
}
void _delay(int32_t _counter)
{
	while(_counter--);
}
void waiting_is_happy()
{
	extern volatile uint8_t UART_received_char;
	while(1)
	{
		if(BUTTON_FUNC_TRUE == 0)
		{
			_delay(1440000);
			if(BUTTON_FUNC_TRUE == 1)
				break;
		}
		if(UART_received_char == 'g')
		{
			break;
		}
	}
}
