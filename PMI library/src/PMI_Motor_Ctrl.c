#include "PMI.h"

/*Init function*/
void PWM_RCC_Init()
{
	RCC_APB2PeriphClockCmd( RCC_APB2Periph_GPIOB | RCC_APB2Periph_TIM1 | RCC_APB2Periph_AFIO | RCC_APB2Periph_GPIOA, ENABLE);
     RCC_APB1PeriphClockCmd( RCC_APB1Periph_TIM2 | RCC_APB1Periph_TIM4, ENABLE);
}
void PWM_GPIO_Init()
{
     GPIO_InitTypeDef    gpio;
	
	/*PWM config*/
	
	gpio.GPIO_Mode = GPIO_Mode_AF_PP;
	gpio.GPIO_Pin = GPIO_PWM_X_PIN;
	gpio.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIO_PWM_X_PORT, &gpio);
	
	gpio.GPIO_Pin = GPIO_PWM_Y_PIN;
	GPIO_Init(GPIO_PWM_Y_PORT, &gpio);
	
	gpio.GPIO_Pin = GPIO_PWM_Z_PIN;
	GPIO_Init(GPIO_PWM_Z_PORT, &gpio);
	
	/*Direction config*/
	
	gpio.GPIO_Mode = GPIO_Mode_Out_OD;
	gpio.GPIO_Pin = GPIO_DIR_X_PIN;
	GPIO_Init(GPIO_DIR_X_PORT, &gpio);
	
	gpio.GPIO_Pin = GPIO_DIR_Y_PIN;
	GPIO_Init(GPIO_DIR_Y_PORT, &gpio);
	
	gpio.GPIO_Pin = GPIO_DIR_Z_PIN;
	GPIO_Init(GPIO_DIR_Z_PORT, &gpio);
	
}
void PWM_Init()
{
     uint16_t Prescaler  = 71; 
     uint16_t PWM_Freq   = 10000; 
     uint16_t Period     = 100;
     uint16_t Pulse      = Period >> 1;
     
     TIM_TimeBaseInitTypeDef	 	 	motor_timer;
     TIM_OCInitTypeDef 				motor_pwm;
     
     PWM_RCC_Init();
     PWM_GPIO_Init();
     
     motor_timer.TIM_ClockDivision  		= 0;
     motor_timer.TIM_CounterMode 		     = TIM_CounterMode_Up;
     motor_timer.TIM_Prescaler 			= Prescaler;
     motor_timer.TIM_Period 				= Period;
     motor_timer.TIM_RepetitionCounter       = 0;

     TIM_TimeBaseInit(MotorX_Timer, &motor_timer);
     TIM_TimeBaseInit(MotorY_Timer, &motor_timer);
     TIM_TimeBaseInit(MotorZ_Timer, &motor_timer);
     
     motor_pwm.TIM_OCMode = TIM_OCMode_PWM1;
     motor_pwm.TIM_OutputState = TIM_OutputState_Enable;
     motor_pwm.TIM_OutputNState = TIM_OutputNState_Enable;
     motor_pwm.TIM_Pulse = Pulse;
     motor_pwm.TIM_OCPolarity = TIM_OCPolarity_Low;
     motor_pwm.TIM_OCNPolarity = TIM_OCNPolarity_High;
     motor_pwm.TIM_OCIdleState = TIM_OCIdleState_Set;
     motor_pwm.TIM_OCNIdleState = TIM_OCIdleState_Reset;

     TIM_OC1Init(MotorX_Timer, &motor_pwm);
     TIM_OC1Init(MotorY_Timer, &motor_pwm);
     TIM_OC1Init(MotorZ_Timer, &motor_pwm);
          
     TIM_CtrlPWMOutputs(MotorX_Timer, ENABLE);   
     TIM_CtrlPWMOutputs(MotorY_Timer, ENABLE);     
     TIM_CtrlPWMOutputs(MotorZ_Timer, ENABLE);
     
     TIM_ClearFlag(MotorX_Timer, TIM_FLAG_Update);        //clr pending flag
     TIM_ClearFlag(MotorY_Timer, TIM_FLAG_Update);
     TIM_ClearFlag(MotorZ_Timer, TIM_FLAG_Update);
     
     TIM_ITConfig(MotorX_Timer,TIM_IT_Update, ENABLE);	
     TIM_ITConfig(MotorY_Timer,TIM_IT_Update, ENABLE);
     TIM_ITConfig(MotorZ_Timer,TIM_IT_Update, ENABLE);
     
     PWM_NVIC_Configuration();
}
void PWM_NVIC_Configuration(void)
{
  NVIC_InitTypeDef NVIC_InitStructure;

  /* Enable the MotorY_Timer Interrupt TIM2_IRQn*/
  NVIC_InitStructure.NVIC_IRQChannel = TIM1_UP_IRQn;/**/
  NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 1;
  NVIC_InitStructure.NVIC_IRQChannelSubPriority = 1;
  NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;

  NVIC_Init(&NVIC_InitStructure);
     
  NVIC_InitStructure.NVIC_IRQChannel = TIM2_IRQn;/**/
  NVIC_InitStructure.NVIC_IRQChannelSubPriority = 2;
  NVIC_Init(&NVIC_InitStructure);
     
  NVIC_InitStructure.NVIC_IRQChannel = TIM4_IRQn;/**/
  NVIC_InitStructure.NVIC_IRQChannelSubPriority = 3;
  NVIC_Init(&NVIC_InitStructure);
}

/* */
void PWM_Enforce_Hcode(hcode hc)
{
       if(hc.step[step_x] < 0)
            MOVING_RIGHT;
       else
            MOVING_LEFT;
       
       if(hc.step[step_y] < 0)
            MOVING_BACKWARD;
       else
            MOVING_FORWARD;
       
       if(hc.step[step_z] < 0)
            MOVING_DOWN;
       else
            MOVING_UP;
           
	  set_step(step_x,hc.step[step_x]);
	  set_step(step_y,hc.step[step_y]);
	  set_step(step_z,hc.step[step_z]);
       
       if(hc.step[0])
            TIM_Cmd(MotorX_Timer, ENABLE);
       if(hc.step[1])
            TIM_Cmd(MotorY_Timer, ENABLE);
       if(hc.step[2])
            TIM_Cmd(MotorZ_Timer, ENABLE);
	  
	  
}
void PWM_Move(int dir)
{
	hcode a;
	switch(dir)
	{
		case _move_left:
			hc_init_vl(&a,0,MOVING_STEP,0,0);
		break;
		
		case _move_right:
			hc_init_vl(&a,0,-MOVING_STEP,0,0);
		break;
		
		case _move_forward:
			hc_init_vl(&a,0,0,-MOVING_STEP,0);
		break;
		
		case _move_backward:
			hc_init_vl(&a,0,0,MOVING_STEP,0);
		break;
		
		case _move_up:
			hc_init_vl(&a,0,0,0,MOVING_STEP);
		break;
		
		case _move_down:
			hc_init_vl(&a,0,0,0,-MOVING_STEP);
		break;
	}
	PWM_Enforce_Hcode(a);
	while(!left_step_equal_zeros());
}

