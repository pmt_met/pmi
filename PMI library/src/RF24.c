//#include "delay.h"
/*
#include "stm32f10x_gpio.h"
#include "stm32f10x_rcc.h"
#include "RF24.h"
*/
#include "PMI.h"
#include "delay.h"
//****************************************************************************************
//*DEFINE VALUE nRF24L01 COMMAND
//***************************************************************************************/
//*********************************************NRF24L01***********************************
#define TX_ADR_WIDTH    5     // 2 uints TX address width
#define RX_ADR_WIDTH    5     // 2 uints RX address width
#define TX_PLOAD_WIDTH  2     // 2 uints TX payload
#define RX_PLOAD_WIDTH  2     // 2 uints TX payload

//***************************************NRF24L01 Command ********************************
#define READ_REG        0x00  // Define read command to register
#define WRITE_REG       0x20  // Define write command to register
#define RD_RX_PLOAD     0x61  // Define RX payload register address
#define WR_TX_PLOAD     0xA0  // Define TX payload register address
#define WR_ACK_PAYLOAD	0xA8
#define FLUSH_TX        0xE1  // Define flush TX register command
#define FLUSH_RX        0xE2  // Define flush RX register command
#define REUSE_TX_PL     0xE3  // Define reuse TX payload register command
#define NOP             0xFF  // Define No Operation, might be used to read status register
//*************************************SPI(nRF24L01)**************************************
#define CONFIG          0x00  // 'Config' register address
#define EN_AA           0x01  // 'Enable Auto Acknowledgment' register address
#define EN_RXADDR       0x02  // 'Enabled RX addresses' register address
#define SETUP_AW        0x03  // 'Setup address width' register address
#define SETUP_RETR      0x04  // 'Setup Auto. Retrans' register address
#define RF_CH           0x05  // 'RF channel' register address
#define RF_SETUP        0x06  // 'RF setup' register address
#define STATUS          0x07  // 'Status' register address
#define OBSERVE_TX      0x08  // 'Observe TX' register address
#define CD              0x09  // 'Carrier Detect' register address
#define RX_ADDR_P0      0x0A  // 'RX address pipe0' register address
#define RX_ADDR_P1      0x0B  // 'RX address pipe1' register address
#define RX_ADDR_P2      0x0C  // 'RX address pipe2' register address
#define RX_ADDR_P3      0x0D  // 'RX address pipe3' register address
#define RX_ADDR_P4      0x0E  // 'RX address pipe4' register address
#define RX_ADDR_P5      0x0F  // 'RX address pipe5' register address
#define TX_ADDR         0x10  // 'TX address' register address
#define RX_PW_P0        0x11  // 'RX payload width, pipe0' register address
#define RX_PW_P1        0x12  // 'RX payload width, pipe1' register address
#define RX_PW_P2        0x13  // 'RX payload width, pipe2' register address
#define RX_PW_P3        0x14  // 'RX payload width, pipe3' register address
#define RX_PW_P4        0x15  // 'RX payload width, pipe4' register address
#define RX_PW_P5        0x16  // 'RX payload width, pipe5' register address
#define FIFO_STATUS     0x17  // 'FIFO Status Register' register address

//**********************************nRF24L01 interrupt flag's*****************************
#define IDLE            0x00  // Idle, no interrupt pending
#define MAX_RT          0x10  // Max #of TX retrans interrupt
#define TX_DS           0x20  // TX data sent interrupt
#define RX_DR           0x40  // RX data received
//****************************************************************************************
//CONFIG register bitwise definitions
#define nrf24l01_CONFIG_RESERVED	0x80
#define	nrf24l01_CONFIG_MASK_RX_DR	0x40
#define	nrf24l01_CONFIG_MASK_TX_DS	0x20
#define	nrf24l01_CONFIG_MASK_MAX_RT	0x10
#define	nrf24l01_CONFIG_EN_CRC		0x08
#define	nrf24l01_CONFIG_CRCO		0x04
#define	nrf24l01_CONFIG_PWR_UP		0x02
#define	nrf24l01_CONFIG_PRIM_RX		0x01
//****************************************************************************************

extern unsigned char TxBuf[TX_PLOAD_WIDTH];
extern unsigned char RxBuf[RX_PLOAD_WIDTH];

unsigned char RxBuf[RX_PLOAD_WIDTH];
unsigned char TxBuf[TX_PLOAD_WIDTH];

void RCC_RF24L01_Init(void);
void IO_RF24L01_initial(void);
void init_NRF24L01(void);
unsigned char nRF24L01_SPI_RW(unsigned char Buff);
unsigned char nRF24L01_SPI_Read(unsigned char reg);
unsigned char nRF24L01_SPI_RW_Reg(unsigned char reg, unsigned char value);
unsigned char nRF24L01_SPI_Read_Buf(unsigned char reg, unsigned char *pBuf, unsigned char uchars);
unsigned char nRF24L01_SPI_Write_Buf(unsigned char reg, unsigned char *pBuf, unsigned uchars);
void nRF24L01_SetRX_Mode(void);
void nRF24L01_SetTX_Mode(void);
void nRF24L01_SetChannel(unsigned int channel);
void nRF24L01_ClearSend(void);
unsigned char nRF24L01_RxPacket(unsigned char* rx_buf);
void nRF24L01_TxPacket(unsigned char* tx_buf);

void nRF24L01_Tx(int* tx_buf);
int* nRF24L01_Rx();
//****************************************************************************************
//*Config ADDRESS RX-TX for RF Device
//***************************************************************************************/
unsigned char TX_ADDRESS[TX_ADR_WIDTH]= {0xA9,0xE6,0xA8,0x74,0xA7};	//
unsigned char RX_ADDRESS[RX_ADR_WIDTH]= {0xA9,0xE6,0xA8,0x74,0xA7};	//

//****************************************************************************************
//*Config PORT
//***************************************************************************************/

/*PORTB*/
#define CSN         GPIO_Pin_3	//cut

//PORTA
#define MOSI        GPIO_Pin_15
#define IRQ         GPIO_Pin_12	//xanh
#define MISO        GPIO_Pin_11
#define SCK         GPIO_Pin_10	//vang
#define CE          GPIO_Pin_9	//trang

#define PORT_RF_OUT     GPIO
#define PORT_RF_DIR     P1DIR
#define PORT_RF_IN      P1IN
//#define PORT_RF_REN     P1REN
//****************************************************************************************
//*Config COMMAND MSP430
//***************************************************************************************/
#define set |= //set state to 1
#define tgl ^= //toggle state 1->0; 0->1
#define clr &=~ //clear state to 0

//****************************************************************************************
// Cac chan su dung cho NRF24L01
//***************************************************************************************/
void RCC_RF24L01_Init(void)
{
     RCC_APB2PeriphClockCmd( RCC_APB2Periph_GPIOB | RCC_APB2Periph_GPIOA, ENABLE);
}
void IO_RF24L01_initial(void)
{
     GPIO_InitTypeDef RF_IO;
     
	// OUPUT: CE SCK CSN MOSI
     RF_IO.GPIO_Mode = GPIO_Mode_Out_PP;
     RF_IO.GPIO_Pin = CE | SCK | MOSI;
     RF_IO.GPIO_Speed = GPIO_Speed_50MHz;
     
     GPIO_Init(GPIOA, &RF_IO);
     
     RF_IO.GPIO_Pin = CSN;
     
     GPIO_Init(GPIOB, &RF_IO);
	// INPUT: IRQ MISO
     RF_IO.GPIO_Mode = GPIO_Mode_IN_FLOATING;
     RF_IO.GPIO_Pin = IRQ | MISO;
     
     GPIO_Init(GPIOA,&RF_IO);
     
	GPIOA->ODR clr CE; //CHIP DISABLE
	GPIOB->ODR set CSN; //CSN IS PULL HIGH.DISABLE THE OPERATION
	GPIOA->ODR clr SCK;//CLK IS LOW

}
//****************************************************************************************
// Cac cai dat ban dau cho NRF24L01
//***************************************************************************************/

void init_NRF24L01(void)
{
     
	delay_us(100);
	RCC_RF24L01_Init();
     IO_RF24L01_initial();

	GPIOA->ODR clr CE;    // chip enable
	GPIOB->ODR set CSN;   // Spi disable
	GPIOA->ODR clr SCK;   // Spi clock line init high

	nRF24L01_SPI_RW_Reg(WRITE_REG + EN_AA, 0x01);     // Turn on auto acknowledge
	nRF24L01_SPI_RW_Reg(WRITE_REG + SETUP_AW, 0x03);  // Address widht 5 bytes
	nRF24L01_SPI_RW_Reg(WRITE_REG + EN_RXADDR, 0x01); // Enable data P0
	nRF24L01_SPI_RW_Reg(WRITE_REG + SETUP_RETR, 0x1a);// 500us + 86us, 10 retrans...
	nRF24L01_SPI_RW_Reg(WRITE_REG + RF_CH, 0);        // Chanel 0 ; RF = 2400 + RF_CH* (1or 2 M)
	nRF24L01_SPI_RW_Reg(WRITE_REG + RF_SETUP, 0x26);  // 1M, 0dbm (0x26 => 250kpbs, 0x07 => 1Mbps, 0x0F => 2Mbps)
	nRF24L01_SPI_RW_Reg(WRITE_REG + RX_PW_P0, RX_PLOAD_WIDTH); // Do rong data truyen 32 byte
	nRF24L01_SPI_Write_Buf(WRITE_REG + TX_ADDR, TX_ADDRESS, TX_ADR_WIDTH);    //
	nRF24L01_SPI_Write_Buf(WRITE_REG + RX_ADDR_P0, RX_ADDRESS, RX_ADR_WIDTH); //
	nRF24L01_SPI_RW_Reg(WRITE_REG + CONFIG, 0x0e);   		 // Enable CRC, 2 byte CRC, Send
	delay_ms(16);
}
//****************************************************************************************************/
// Doc va ghi 1 byte qua SPI
//****************************************************************************************************/
unsigned char nRF24L01_SPI_RW(unsigned char Buff)
{
	unsigned char bit_ctr;
	GPIOA->ODR clr SCK;
	delay_us(1);
	for(bit_ctr=0;bit_ctr<8;bit_ctr++) // output 8-bit
	{
		if (Buff & 0x80)
			GPIOA->ODR set MOSI;
		else
			GPIOA->ODR clr MOSI;			// output 'byte', MSB to MOSI

		GPIOA->ODR set SCK;       // Set SCK high..
		delay_us(2);
		Buff = (Buff << 1);           // shift next bit into MSB..
		delay_us(5);

          if(GPIOA->IDR & MISO)
		//if (P2IN & (MISO)) // capture current MISO bit
			Buff |= 0x01;
		else
		{
			Buff &= ~0x01;
		}
		GPIOA->ODR clr SCK;            		  // ..then set SCK low again
		delay_us(2);
   	}
    return(Buff);           		  // return read uchar
}
//****************************************************************************************************/
// Doc thanh ghi reg cua NRF24L01
//****************************************************************************************************/
unsigned char nRF24L01_SPI_Read(unsigned char reg)
{
	unsigned char reg_val;

	GPIOB->ODR clr CSN;                // CSN low, initialize SPI communication...
	nRF24L01_SPI_RW(reg);            // Select register to read from..
	reg_val = nRF24L01_SPI_RW(0);    // ..then read registervalue
	GPIOB->ODR set CSN;                // CSN high, terminate SPI communication

	return(reg_val);        // return register value
}
//****************************************************************************************************/
// Ghi gia tri value vao thanh ghi reg
//****************************************************************************************************/
unsigned char nRF24L01_SPI_RW_Reg(unsigned char reg, unsigned char value)
{
	unsigned char status;

	GPIOB->ODR clr CSN;                   // CSN low, init SPI transaction
	status = nRF24L01_SPI_RW(reg);      // select register
	nRF24L01_SPI_RW(value);             // ..and write value to it..
	GPIOB->ODR set CSN;                   // CSN high again

	return(status);            // return nRF24L01 status uchar
}
//****************************************************************************************************/
// Doc uchars byte cua thanh ghi reg va luu vao pBuf
//****************************************************************************************************/
unsigned char nRF24L01_SPI_Read_Buf(unsigned char reg, unsigned char *pBuf, unsigned char uchars)
{
	unsigned char status,uchar_ctr;

	GPIOB->ODR clr CSN;                   		// Set CSN low, init SPI tranaction
	status = nRF24L01_SPI_RW(reg);       		// Select register to write to and read status uchar

	for(uchar_ctr=0;uchar_ctr<uchars;uchar_ctr++)
		pBuf[uchar_ctr] = nRF24L01_SPI_RW(0);    //

	GPIOB->ODR set CSN;

	return(status);                    // return nRF24L01 status uchar
}
//****************************************************************************************************/
// ghi uchar byte cua du lieu pBuf vao thanh ghi reg
//****************************************************************************************************/
unsigned char nRF24L01_SPI_Write_Buf(unsigned char reg, unsigned char *pBuf, unsigned uchars)
{
	unsigned char status,uchar_ctr;
	GPIOB->ODR clr CSN;            //SPI
	status = nRF24L01_SPI_RW(reg);
	for(uchar_ctr=0; uchar_ctr<uchars; uchar_ctr++) //
		nRF24L01_SPI_RW(*pBuf++);
	GPIOB->ODR set CSN;           //SPI
	return(status);    //
}
//****************************************************************************************************/
// Che do nhan data
//****************************************************************************************************/
void nRF24L01_SetRX_Mode(void)
{
	GPIOA->ODR clr CE;
	nRF24L01_SPI_RW_Reg(WRITE_REG + CONFIG, 0x0f); // Enable CRC, 2 byte CRC, Recive
	GPIOA->ODR set CE;
	delay_ms(16);
}
//****************************************************************************************************/
// Che do truyen data
//****************************************************************************************************/
void nRF24L01_SetTX_Mode(void)
{
	GPIOB->ODR clr CE;
	nRF24L01_SPI_RW_Reg(WRITE_REG + CONFIG, 0x0e); // Enable CRC, 2 byte CRC, Send
	nRF24L01_SPI_RW_Reg(WRITE_REG + SETUP_RETR,0x2F); //Setup resend
}

//****************************************************************************************************/
// Cai dat kenh truyen nhan
//****************************************************************************************************/
void nRF24L01_SetChannel(unsigned int channel)
{
	if(channel > 127) channel = 127;
	nRF24L01_SPI_RW_Reg(WRITE_REG + RF_CH, channel);
}

//****************************************************************************************************/
// Xoa co ngat
//****************************************************************************************************/
void nRF24L01_ClearSend(void)
{
	nRF24L01_SPI_RW_Reg(WRITE_REG+STATUS,0XFF);
}

//****************************************************************************************************/
// Doc du lieu va luu vao rx_buf
//****************************************************************************************************/
unsigned char nRF24L01_RxPacket(unsigned char* rx_buf)
{
    unsigned char revale=0;
    unsigned char sta;
    sta=nRF24L01_SPI_Read(STATUS);	// Read Status
    if(sta&0x40)		// Data in RX FIFO
	{
          GPIOA->ODR clr CE; 			//SPI
		nRF24L01_SPI_Read_Buf(RD_RX_PLOAD,rx_buf,TX_PLOAD_WIDTH);// read receive payload from RX_FIFO buffer
		revale =1;
		GPIOA->ODR set CE;
	}
	nRF24L01_SPI_RW_Reg(WRITE_REG+STATUS,sta);
	return revale;
}
//****************************************************************************************************/
// Gui du lieu tx_buf di
//****************************************************************************************************/
void nRF24L01_TxPacket(unsigned char * tx_buf)
{
	GPIOA->ODR clr CE; // Standby 1
	nRF24L01_SPI_Write_Buf(WRITE_REG + RX_ADDR_P0, TX_ADDRESS, TX_ADR_WIDTH); // Send Address
	nRF24L01_SPI_Write_Buf(WR_TX_PLOAD, tx_buf, TX_PLOAD_WIDTH); 	          // Send data

	GPIOA->ODR set CE; // Set CE 10us to Transmit
	delay_us(10);
	GPIOA->ODR clr CE; // Standby 1
}
//****************************************************************************************************/
// int tx_buf[2] = {,};
//****************************************************************************************************/
void nRF24L01_Tx(int* tx_buf)
{

}
//****************************************************************************************************/
//
//****************************************************************************************************/
/*int* nRF24L01_Rx()
{
	int rx_buf[2];
	return rx_buf;
}*/

//****************************************************************************************************/
//
//****************************************************************************************************/
