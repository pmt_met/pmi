#include "PMI.h"
//#include "stm32f10x_it.h"
/**/
void UART_RCC_INIT()
{
     RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA | RCC_APB2Periph_AFIO | RCC_APB2Periph_USART1, ENABLE);
}
/**/
void UART_GPIO_INIT()
{    
     GPIO_InitTypeDef GPIO_InitStructure;
    	/* Configure PA9 for USART Tx as alternate function push-pull */
	GPIO_InitStructure.GPIO_Pin = SERIAL_Tx_Pin;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOA, &GPIO_InitStructure);

	/* Configure PA10 for USART Rx as input floating */
	GPIO_InitStructure.GPIO_Pin = SERIAL_Rx_Pin;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
	GPIO_Init(GPIOA, &GPIO_InitStructure);	
}
void UART_NVIC_Configuration(void)
{
  NVIC_InitTypeDef NVIC_InitStructure;

  /* Configure the NVIC Preemption Priority Bits */  
  NVIC_PriorityGroupConfig(NVIC_PriorityGroup_0);
  
  /* Enable the USARTy Interrupt */
  NVIC_InitStructure.NVIC_IRQChannel = USART1_IRQn;
  NVIC_InitStructure.NVIC_IRQChannelSubPriority = 5;
  NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
  NVIC_Init(&NVIC_InitStructure);

}
/**/
void UART_INIT()
{
     USART_InitTypeDef USART_InitStructure;
     /*--------------------------------------------------------*/
     
     USART_DeInit(SERIAL);
     UART_RCC_INIT();
     UART_GPIO_INIT();
     UART_NVIC_Configuration();
     //USART_StructInit(&USART_InitStructure);
     
	USART_InitStructure.USART_BaudRate = 9600;
	USART_InitStructure.USART_WordLength = USART_WordLength_8b;
	USART_InitStructure.USART_StopBits = USART_StopBits_1;
	USART_InitStructure.USART_Parity = USART_Parity_No;
	USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
	USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;	

	/* USART configuration */
	USART_Init(SERIAL, &USART_InitStructure);
	/**/
	USART_ITConfig(USART1, USART_IT_RXNE, ENABLE);
	/* Enable USART */
	USART_Cmd(SERIAL, ENABLE);	
	
	
     /*--------------------------------------------------------*/
}

/**/
uint16_t UART_GetChar()
{
     //char Data;
	while(USART_GetFlagStatus(SERIAL, USART_FLAG_RXNE) == RESET);
	return (uint16_t)USART_ReceiveData(SERIAL);
	//return Data;
}
void UART_GetString(char *input)
{
	uint8_t i = 0;	
	uint8_t temp;
	
	while(1)
	{
		temp = UART_GetChar();
		if(temp == 0xD)
			break;
		else 
		{
			input[i] = temp;
			i++;	
		}
	}
	//i = 0;
}
void UART_SendChar(char Data)
{
     USART_SendData(USART1,Data);

  /* Loop until the end of transmission */
  while (USART_GetFlagStatus(SERIAL, USART_FLAG_TC) == RESET)
  {}

}
void UART_SendStr(char *Data)
{
     while(*Data)
     {
          UART_SendChar(*Data++);
     }
}
void UART_SendNumb(int Data)
{
     char temp[10] = "";
     sprintf(temp, "%d", Data);
     UART_SendStr((char *)temp);
}
GETCHAR_PROTOTYPE
{
  return UART_GetChar();
}

PUTCHAR_PROTOTYPE
{
  /* Place your implementation of fputc here */
  /* e.g. write a character to the USART */
   USART_SendData(SERIAL,ch);

  /* Loop until the end of transmission */
  while (USART_GetFlagStatus(SERIAL, USART_FLAG_TC) == RESET)
  {}

  return ch;
}
