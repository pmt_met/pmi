#include "PMI.h"

FATFS fsa;        /* Work area (file system object) for logical drive */
FIL fsrc;         /* file objects */   
FRESULT res;

int Current_status = -2;
extern volatile uint8_t UART_received_char;
int main(void)
{
     /*Matumba variable*/
     int i = 0;
     /*Manual variable*/
	hcode a;
     /*Init funtion*/
     Extern_funtion_init();
     PWM_Init();
	Init_motor_position();
     UART_INIT();

	/*move spindle to right position*/
	UART_SendStr("\n HANDJOB");
	
	
	while(UART_received_char != 'g')
	{
		DoYourHandJob();
	}
	
	
	/*Matubaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa*/
	UART_SendStr("\n MATUMBA");
	DoYourMatumba();
	f_close(&fsrc);  
     
     UART_SendStr("\n Done!!!");
	return 0;  
}

uint16_t DoYourHandJob()
{
	char feedback = 0x00;
	switch(UART_received_char)
	{
		case 'w':
			PWM_Move(_move_backward);
		break;
		
		case 'x':
			PWM_Move(_move_forward);
		break;
		
		case 'a':
			PWM_Move(_move_left);
		break;
		
		case 'd':
			PWM_Move(_move_right);
		break;
		
		case 'u':
			PWM_Move(_move_up);
		break;
		
		case 'j':
			PWM_Move(_move_down);
		break;
		
		case 'f':
		case 'y':
		case 'h':
		break;
		default:
			TIM_Cmd(MotorX_Timer, DISABLE);
			TIM_Cmd(MotorY_Timer, DISABLE);
			TIM_Cmd(MotorZ_Timer, DISABLE);
		break;

	}
	feedback = UART_received_char;
	UART_received_char = 0x00;
	return feedback;
}
void DoYourMatumba()
{
	hcode a;
     bool done = false;
     int i = 0;
     char buffer[50][30];
	
	f_mount(0,&fsa);	
	
	res = f_open( &fsrc , "0:/hcode.hc" , FA_READ);
	
	/*******************Wait for start button********************/
	waiting_is_happy();
	
	/*******************Detect z axis zeros position********************/
	if(AUTO_Z)
	{
		if ( res == FR_OK )
		{   
			f_gets(buffer[i],30,&fsrc);
		}
	}
			
	while(!done)
	{
		if ( res == FR_OK )
		{   
			for(i = 0; i < 50; i++)  
			{
				f_gets(buffer[i],30,&fsrc);
			}
			for(i = 0; i < 50; i++)
			{
				hc_init(&a);
				a = hc_string_to_hcode(buffer[i]);
				
				if(a.action == IN_THE_END)
				{
					done = true;
					break;
				}
				Current_status = a.action;
					switch(a.action)
				{
					case ACT_DRILL:	
						UART_SendStr("\n Drill");
						Change_tool();
						break;
					
					case ACT_MILL:		
						UART_SendStr("\n Mill");
						Change_tool();
						break;
						
					case ACT_CUT:		
						UART_SendStr("\n Cut");
						Change_tool();
						break;
						
					case ACT_SIDE:
						UART_SendStr("\n Flip");
						Change_tool();
						break;
					
					case ACT_Z_LEVELING:
						UART_SendStr("\n Z leveling");
						hc_init_vl(&a,0,0,0,0);
						Auto_z_leveling_Point(a,false);
						
						waiting_is_happy();
						break;
					
					/*I'm a simple switch, i see there command and i just breakkkkkkkkkkk */
					case ACT_NORMAL:	break;
					case ACT_FASTMOVE:	break;
					case ACT_STOP:		break;
					default: 			break;
				}
				/*Just do this*/
				PWM_Enforce_Hcode(a);
				while(!left_step_equal_zeros());
			}
			i = 0;
		}
	}
}