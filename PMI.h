#ifndef PMI_H
#define PMI_H


#include <stdio.h>
#include <stdlib.h>
#include "stdbool.h"
#include "string.h"

#include "PMI_Hcode.h"
#include "PMI_Motor_Ctrl.h"
#include "PMI_Serial.h"
#include "PMI_Extern_funtion.h"
#include "RF24.h"
#include "hpdragon_spi_sd.h"

#include "stm32f10x_it.h"
#include "stm32f10x_gpio.h"
#include "stm32f10x_rcc.h"
#include "stm32f10x_usart.h"
#include "stm32f10x_tim.h"

#include "diskio.h"
#include "ff.h"
#include "integer.h"


#define AUTO_Z_SAMPLING_STEP		25600//32768 = 2^15
#define AUTO_Z_STEP_Z			12800
#define Z_HIGHT_CHANGETOOL		51200	//25mm	
/*Variable*/


/*funtion*/
uint16_t DoYourHandJob();
void DoYourMatumba();
#endif /*PMI_H*/
