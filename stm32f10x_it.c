/**
  ******************************************************************************
  * @file    Project/STM32F10x_StdPeriph_Template/stm32f10x_it.c 
  * @author  MCD Application Team
  * @version V3.5.0
  * @date    08-April-2011
  * @brief   Main Interrupt Service Routines.
  *          This file provides template for all exceptions handler and 
  *          peripherals interrupt service routine.
  ******************************************************************************
  * @attention
  *
  * THE PRESENT FIRMWARE WHICH IS FOR GUIDANCE ONLY AIMS AT PROVIDING CUSTOMERS
  * WITH CODING INFORMATION REGARDING THEIR PRODUCTS IN ORDER FOR THEM TO SAVE
  * TIME. AS A RESULT, STMICROELECTRONICS SHALL NOT BE HELD LIABLE FOR ANY
  * DIRECT, INDIRECT OR CONSEQUENTIAL DAMAGES WITH RESPECT TO ANY CLAIMS ARISING
  * FROM THE CONTENT OF SUCH FIRMWARE AND/OR THE USE MADE BY CUSTOMERS OF THE
  * CODING INFORMATION CONTAINED HEREIN IN CONNECTION WITH THEIR PRODUCTS.
  *
  * <h2><center>&copy; COPYRIGHT 2011 STMicroelectronics</center></h2>
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "stm32f10x_it.h"
//#include "PMI_Hcode.h"
//#include "PMI_Motor_Ctrl.h"
//#include "hpdragon_led.h"
/** @addtogroup STM32F10x_StdPeriph_Template
  * @{
  */

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
//int32_t StepLeftMotor[3] = {0,0,0};
int32_t StepCounterMotor[3] = {0,0,0};
int32_t Motor_Postion[3] = {0,0,0};
int32_t Auto_z_counter = 0;
volatile bool Z_axis_get_zeros = false;
volatile uint8_t UART_received_char = 0x00;
//int32_t x_potion = 0;
//int32_t y_potion = 0;
/* Private function prototypes -----------------------------------------------*/
/* Private functions ---------------------------------------------------------*/

/******************************************************************************/
/*            Cortex-M3 Processor Exceptions Handlers                         */
/******************************************************************************/

/**
  * @brief  This function handles NMI exception.
  * @param  None
  * @retval None
  */
void NMI_Handler(void)
{
}

/**
  * @brief  This function handles Hard Fault exception.
  * @param  None
  * @retval None
  */
void HardFault_Handler(void)
{
  /* Go to infinite loop when Hard Fault exception occurs */
  while (1)
  {
	  UART_SendStr("\nHardFault");
  }
}

/**
  * @brief  This function handles Memory Manage exception.
  * @param  None
  * @retval None
  */
void MemManage_Handler(void)
{
  /* Go to infinite loop when Memory Manage exception occurs */
  while (1)
  {
  }
}

/**
  * @brief  This function handles Bus Fault exception.
  * @param  None
  * @retval None
  */
void BusFault_Handler(void)
{
  /* Go to infinite loop when Bus Fault exception occurs */
  while (1)
  {
  }
}

/**
  * @brief  This function handles Usage Fault exception.
  * @param  None
  * @retval None
  */
void UsageFault_Handler(void)
{
  /* Go to infinite loop when Usage Fault exception occurs */
  while (1)
  {
  }
}

/**
  * @brief  This function handles SVCall exception.
  * @param  None
  * @retval None
  */
void SVC_Handler(void)
{
}

/**
  * @brief  This function handles Debug Monitor exception.
  * @param  None
  * @retval None
  */
void DebugMon_Handler(void)
{
}

/**
  * @brief  This function handles PendSVC exception.
  * @param  None
  * @retval None
  */
void PendSV_Handler(void)
{
}

/**
  * @brief  This function handles SysTick Handler.
  * @param  None
  * @retval None
  */
void SysTick_Handler(void)
{
}

/******************************************************************************/
/*                 STM32F10x Peripherals Interrupt Handlers                   */
/*  Add here the Interrupt Handler for the used peripheral(s) (PPP), for the  */
/*  available peripheral interrupt handler's name please refer to the startup */
/*  file (startup_stm32f10x_xx.s).                                            */
/******************************************************************************/
//extern int Current_status;
void EXTI15_10_IRQHandler(void)
{
	extern int Current_status;
     if(EXTI_GetITStatus(EXTI_Line15) != RESET)
     {
		if(GPIO_DIR_Z_PORT->ODR & GPIO_DIR_Z_PIN && Current_status == ACT_Z_LEVELING)
		{
			TIM_Cmd(MotorZ_Timer, DISABLE);
			Z_axis_get_zeros = true;
		}
		EXTI_ClearFlag(EXTI_Line15);
     }
}

void EXTI4_IRQHandler(void)
{
	if(EXTI_GetITStatus(EXTI_Line4) != RESET)
     {
		TIM_Cmd(MotorX_Timer,DISABLE);
		TIM_Cmd(MotorY_Timer,DISABLE);
		TIM_Cmd(MotorZ_Timer,DISABLE);
		
		Back_to_zeros_position();
		EXTI_ClearFlag(EXTI_Line4);
     }
	exit(0);
}

void USART1_IRQHandler(void)
{
	if(USART_GetITStatus(USART1, USART_IT_RXNE) != RESET)
	{
		UART_received_char = UART_GetChar();
	}
	USART_ClearFlag(USART1, USART_IT_RXNE);
}

/**
  * @brief  This function handles PPP interrupt request.
  * @param  None
  * @retval None
  */
/*void PPP_IRQHandler(void)
{
}*/

/**
  * @}
  */ 
void TIM1_UP_IRQHandler(void)/*X axis's motor*/
{    
	if (TIM_GetITStatus(MotorX_Timer, TIM_IT_Update) != RESET)
	{
		if(GPIO_DIR_X_PORT->ODR&GPIO_DIR_X_PIN)
			Motor_Postion[step_x]++;
		else
			Motor_Postion[step_x]--;
		
		
			if(StepCounterMotor[step_x] > 0)
			{
				StepCounterMotor[step_x]--;	
			}
			else if(StepCounterMotor[step_x] < 0)
			{
				StepCounterMotor[step_x]++;
			}
			if(0 == StepCounterMotor[step_x])
			{
				TIM_Cmd(MotorX_Timer, DISABLE);
			}
		
		TIM_ClearITPendingBit(MotorX_Timer, TIM_IT_Update);
	}
	
}

void TIM2_IRQHandler(void)/*Y axis's motor*/
{
	
	if (TIM_GetITStatus(MotorY_Timer, TIM_IT_Update) != RESET)
	{
		if(GPIO_DIR_Y_PORT->ODR&GPIO_DIR_Y_PIN)
			Motor_Postion[step_y]++;
		else
			Motor_Postion[step_y]--;
		
		if(StepCounterMotor[step_y] > 0)
		{
			StepCounterMotor[step_y]--;	
		}
		else if(StepCounterMotor[step_y] < 0)
		{
			StepCounterMotor[step_y]++;
		}
		if(0 == StepCounterMotor[step_y])
		{
			TIM_Cmd(MotorY_Timer, DISABLE);
		}    
		
		TIM_ClearITPendingBit(MotorY_Timer, TIM_IT_Update);
	}
}
void TIM4_IRQHandler(void)/*Z axis's motor*/
{
	
	if (TIM_GetITStatus(MotorZ_Timer, TIM_IT_Update) != RESET)
	{
		if(GPIO_DIR_Z_PORT->ODR&GPIO_DIR_Z_PIN)
			Motor_Postion[step_z]--;
		else
			Motor_Postion[step_z]++;
		
		
			Auto_z_counter++;
			if(StepCounterMotor[step_z] > 0)
			{
				StepCounterMotor[step_z]--;	
			}	
			else if(StepCounterMotor[step_z] < 0)
			{
				StepCounterMotor[step_z]++;
			}
			if(0 == StepCounterMotor[step_z])
			{
				TIM_Cmd(MotorZ_Timer, DISABLE);
			}  
		
		TIM_ClearITPendingBit(MotorZ_Timer, TIM_IT_Update);
	}
}
void set_step(int8_t motor_numb, uint32_t step)
{
     StepCounterMotor[motor_numb] = step;
}
int32_t left_step_equal_zeros()
{
	if( 0 == StepCounterMotor[step_x] && 0 == StepCounterMotor[step_y] && 0 == StepCounterMotor[step_z])
	{
		return 1;
	}
     return 0;
}

/*	            
			A-----D------------B
			|	a|            | 
			|	 |	  b	    |
			H-----X------------F
			|  d  |            |
			|     |            | 
			|     |c           |
			|     |            |
			|	 |            |
			|	 |		    |
			D-----G------------C
			
				A(a + d) + B(a + b) + C(b + c) + D(c + d)
			X = --------------------------------------------
							    4
				
		
		x = Get_current_position(step_x);
		y = Get_current_position(step_y);
		
		x = Get_current_position(step_x);
		y = Get_current_position(step_y);
		
		row_mod = (x / AUTO_Z_SAMPLING_STEP);
		col_mod = (y / AUTO_Z_SAMPLING_STEP);
		
		row_div = (x % AUTO_Z_SAMPLING_STEP);
		col_div = (y % AUTO_Z_SAMPLING_STEP);
		
		A = matrix[row_div][col_div];
		B = matrix[row_div][col_div + 1];
		C = matrix[row_div + 1][col_div + 1];
		D = matrix[row_div + 1][col_div];
		
		a = row_div;
		c = 1 - a;
		d = col_div;
		b = 1 - d;	
		
		offset = (A(a + d) + B(a + b) + C(b + c) + D(c + d))>>2;		
		*/
int calculation_Zaxis_offset()
{
//	int row_div, col_div, A, B, C, D, a, b, c, d, offset;
//	float row_mod, col_mod;
//	
//	row_mod = (Motor_Postion[step_x] / AUTO_Z_SAMPLING_STEP);
//	col_mod = (Motor_Postion[step_y] / AUTO_Z_SAMPLING_STEP);
//	
//	row_div = (Motor_Postion[step_x] % AUTO_Z_SAMPLING_STEP);
//	col_div = (Motor_Postion[step_y] % AUTO_Z_SAMPLING_STEP);
//	
//	A = get_matrix(row_div,col_div);
//	B = get_matrix(row_div,col_div + 1);
//	C = get_matrix(row_div + 1,col_div + 1);
//	D = get_matrix(row_div + 1,col_div);
//	
//	a = row_div;
//	c = 1 - a;
//	d = col_div;
//	b = 1 - d;	
//	
//	offset = (A*(a + d) + B*(a + b) + C*(b + c) + D*(c + d))>>2;
//	return offset;
}

void z_reset()
{
	Z_axis_get_zeros = false;
	Auto_z_counter = 0;
}

void Init_motor_position()
{
	Motor_Postion[0] = Motor_Postion[1] = Motor_Postion[2] = 0;
}


/******************* (C) COPYRIGHT 2011 STMicroelectronics *****END OF FILE****/
