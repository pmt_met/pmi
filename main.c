/**
  ******************************************************************************
  * @file    	main.c
  * @author  	PHC
  * @version 	V1.0.0
  * @date    	16/10/2012
  * @brief   	Main program body.
  ******************************************************************************
  * @copy
  *
  * THE PRESENT FIRMWARE WHICH IS FOR GUIDANCE ONLY AIMS AT PROVIDING CUSTOMERS
  * WITH CODING INFORMATION REGARDING THEIR PRODUCTS IN ORDER FOR THEM TO SAVE
  * TIME. AS A RESULT, STMICROELECTRONICS SHALL NOT BE HELD LIABLE FOR ANY
  * DIRECT, INDIRECT OR CONSEQUENTIAL DAMAGES WITH RESPECT TO ANY CLAIMS ARISING
  * FROM THE CONTENT OF SUCH FIRMWARE AND/OR THE USE MADE BY CUSTOMERS OF THE
  * CODING INFORMATION CONTAINED HEREIN IN CONNECTION WITH THEIR PRODUCTS.
  *
  * 
  */ 

  
/*
*************************************************************************************************************************************
*															INCLUDED FILES															*
*************************************************************************************************************************************
*/

#include "stm32f10x.h"
#include "diskio.h"
#include "ff.h"
#include "integer.h"

#include "hpdragon_led.h"

/*
*************************************************************************************************************************************
*															PRIVATE DEFINE															*
*************************************************************************************************************************************
*/

	/* Private variables ---------------------------------------------------------*/
FATFS fsa;         /* Work area (file system object) for logical drive */
FIL fsrc;         /* file objects */   
FRESULT res;
UINT br;
char path[512]="0:";
uint8_t textFileBuffer[] = "OK Let's get start with this text file. Let's try some Vietnamese words: Xin Chào, tôi là CU\r\n";   

uint8_t i;
/*
*************************************************************************************************************************************
*														 	DATA TYPE DEFINE															*
*************************************************************************************************************************************
*/


/*
*************************************************************************************************************************************
*													   		PRIVATE VARIABLES														*
*************************************************************************************************************************************
*/ 
GPIO_InitTypeDef GPIO_InitStructure;


/*
*************************************************************************************************************************************
*							  								LOCAL FUNCTIONS															*
*************************************************************************************************************************************
*/
/**
  * @brief  	Configures the different system clocks.
  * @param  	None
  * @retval 	None
  */
void RCC_Configuration(void)
{
	/* Setup the microcontroller system. Initialize the Embedded Flash Interface,  
	initialize the PLL and update the SystemFrequency variable. */
	//SystemInit();

	/* Enable GPIOB clocks */
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB, ENABLE); //Cap xung clock cho GPIOB hoat dong
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE); //Cap xung clock cho GPIOA hoat dong
}

/**
  * @brief  	Inserts a delay time with resolution is 10 milisecond..
  * @param  	nCount: specifies the delay time length.
  * @retval 	None
  */
void Delay_ms(__IO uint32_t num)
{
	__IO uint32_t index = 0;

	/* default system clock is 72MHz */
	for(index = (7200 * num); index != 0; index--)
	{
	}
}

void Delay_us(__IO uint32_t num)
{
	__IO uint32_t index2 = 0;

	/* default system clock is 72MHz */
	for(index2 = (7.2 * num); index2 != 0; index2--)
	{
	}
}

void SysTick_Handler(void)
{
	static uint16_t cnt=0;
	static uint8_t cntdiskio=0;

	if( cnt++ >= 500 ) {
		cnt = 0;
		/* alive sign */
	}
	if ( cntdiskio++ >= 10 ) {
		cntdiskio = 0;
		disk_timerproc();
	}

}

/*
*************************************************************************************************************************************
*															GLOBAL FUNCTIONS														*
*************************************************************************************************************************************
*/
/**
  * @brief  	Main program.
  * @param  	None
  * @retval 	None
  */
int main2(void)
{
     char buff[255];
	/*!< At this stage the microcontroller clock setting is already configured, 
       this is done through SystemInit() function which is called from startup
       file (startup_stm32f10x_xx.s) before to branch to application main.
       To reconfigure the default setting of SystemInit() function, refer to
       system_stm32f10x.c file
     */
      
	/* Configure the system clocks */
	RCC_Configuration();
	LED_Init();

	if (SysTick_Config(SystemCoreClock / 1000))
	{
		/* Capture error */
		while (1);
	}

     f_mount(0,&fsa);	

	res = f_open( &fsrc , "0:/hcode.hc" , FA_READ);		

    if ( res == FR_OK )
    { 
         f_gets(buff,255,&fsrc);

      /* Write buffer to file */
//		f_puts("Test ghi dữ liệu\n\n",&fsrc);
//		f_puts((char *)textFileBuffer,&fsrc);
//		
//		LED_OS_Port->ODR &= ~LED_OS_Pin;	
	/*
         
	  for (i=0;i<100;i++)
	  	{
			f_printf(&fsrc, "D? li?u h�ng %i:%i",i,i); 
			f_puts("\n", &fsrc);

	  	}
	*/
      /*close file */
      f_close(&fsrc);      
    }

	while (1)
	{
		
	}
}




/*****END OF FILE****/
